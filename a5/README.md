
# LIS3781

## Nicholas Koester

### Assignment 5 Requirements:

*Three Parts:*

1. Screenshot of ERD
2. Optional: SQL code for the required reports.
3. Bitbucket repo links: *Your*lis3781 Bitbucket repo link

#### Assignment Screenshots:

*Screenshot of SQL ERD for A5*:

![SQL ERD Screenshot](img/A5_ERD.png "SQL ERD for A5")







