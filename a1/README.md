> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781

## Nicholas Koester

### Assignment 1 Requirements:

*Five Parts:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installations
3. Questions
4. Entity Relationship Diagram, and SQL Code
5. Bitbucket repo links:

> #### Git commands w/short descriptions:

1. git init - Creates an empty Git repository
2. git status - Shows the working tree status
3. git add - Add file contents to the index
4. git commit - Record changes to the repository
5. git push - Update remote refs along with associated objects
6. git pull - Fetch from and integrate with another repository or a local branch
7. git clone - Clone a repository into a new directory

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png "ampps running on local machine")

*Screenshot of ERD*:

![ERD Screenshot](img/ERD.png "Completed ERD")

*Screenshot of Question 1*:
![Question Screenshot](img/question1.png "Question 1 completed")

*Screenshot of Question 2*:
![Question Screenshot](img/question2.png "Question 2 completed")

*Screenshot of Question 3*:
![Question Screenshot](img/question3.png "Question 3 completed")

*Screenshot of Question 4*:
![Question Screenshot](img/question4.png "Question 4 completed")

*Screenshot of Question 5*:
![Question Screenshot](img/question5.png "Question 5 completed")

*Screenshot of Question 6*:
![Question Screenshot](img/question6.png "Question 6 completed")

*Screenshot of Question 7*:
![Question Screenshot](img/question7.png "Question 7 completed")



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")


