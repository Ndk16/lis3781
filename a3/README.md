
# LIS3781

## Nicholas Koester

### Assignment 3 Requirements:

*Two Parts:*

1. Screenshot of Oracle SQL code
2. Screenshot of my Oracle populated tables

#### Assignment Screenshots:

*Screenshot of my SQL code from Oracle Part 1*:

![SQL code Screenshot 1](img/SQLCode_1.png "SQL code from oracle 1 of 3")

*Screenshot of my SQL code from Oracle Part 2*:

![SQL code Screenshot 2](img/SQLCode_2.png "SQL code from oracle 2 of 3")

*Screenshot of my SQL code from Oracle Part 3*:

![SQL code Screenshot 3](img/SQLCode_3.png "SQL code from oracle 3 of 3")

*Screenshot of populated tables*:

![populated tables Screenshot](img/tables.png "Populated tables")







