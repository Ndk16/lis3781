> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# Class number and name

## Your name

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Distributed Version Control with Git and Bitbucket
    - AMPPS Installations
    - Questions
    - Entity Relationship Diagram, and SQL Code
    - Bitbucket repo links:

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of SQL code
    - Screenshot of my populated tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Screenshot of Oracle SQL code
    - Screenshot of my Oracle populated tables

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Screenshot of ERD
    - A4 SQL code

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of ERD
    - A5 SQL code

6. [P1 README.md](p1/README.md "My p1 README.md file")
    - Screenshot of Project 1 ERD 

