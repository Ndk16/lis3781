
# LIS3781

## Nicholas Koester

### Assignment 2 Requirements:

*Two Parts:*

1. Screenshot of SQL code
2. Screenshot of my populated tables

#### Assignment Screenshots:

*Screenshot of my SQL code for company table*:

![SQL code Screenshot 1](img/SQLCode_1.png "SQL code for company")


*Screenshot of my SQL code for customer table*:

![SQL code Screenshot 2](img/SQLCode_2.png "SQL code for table")

*Screenshot of populated tables*:

![populated tables Screenshot](img/tables.png "Populated tables")







